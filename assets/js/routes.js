define(['./app'], function (app) {
    'use strict';
    return app.config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "assets/partials/menu.html",
                controller: 'AppCtrl'
            })

            .state('app.home', {
                url: "/home",
                views: {
                    'menuContent': {
                        templateUrl: "assets/partials/home.html",
                        controller: 'HomeCtrl'
                    }
                }
            })

            .state('app.about', {
                url: "/about",
                views: {
                    'menuContent': {
                        templateUrl: "assets/partials/about.html",
                        controller: 'AboutCtrl'
                    }
                }
            });

        $urlRouterProvider.otherwise('/app/home');
    });
});